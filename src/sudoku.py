def solve(board):

    from satispy import Variable, Cnf
    from satispy.solver import Minisat
    from satispy.io import DimacsCnf

    formula = Cnf()

    # var[i][j][k]: i行j列目に数字kを書くかどうか
    var = [[[Variable("%d %d %d" % (i, j, k)) for k in range(10)] for j in range(9)] for i in range(9)]
    
    # すでに数字が書かれているマス
    for i in range(9):
        for j in range(9):
            if board[i][j] != '0':
                formula &= var[i][j][int(board[i][j])]

    import itertools
    # 各マスには高々ひとつの数字
    for i in range(9):
        for j in range(9):
            for k1, k2 in itertools.combinations(range(1, 10), 2):
                formula &= -var[i][j][k1] | -var[i][j][k2]
    # 各行にはすべての数字が現れる
    for i in range(9):
        for k in range(1, 10):
            clause = Cnf()
            for j in range(9):
                clause |= var[i][j][k]
            formula &= clause
    # 各列にはすべての数字が現れる
    for j in range(9):
        for k in range(1, 10):
            #TODO
    # 各3x3ブロックにすべての数字が現れる
    #TODO
    solver = Minisat()
    solution = solver.solve(formula)

    print('== solution ==')
    for i in range(9):
        for j in range(9):
            for k in range(1, 10):
                if solution[var[i][j][k]]:
                    print(k, end='')
        print()    

if __name__ == "__main__":
    line = input()
    board = []
    for i in range(9):
        board.append(line[9*i:9*i+9])
    solve(board)
def solve(N):

    from satispy import Variable, Cnf
    from satispy.solver import Minisat
    from satispy.io import DimacsCnf

    formula = Cnf()

    # var[i][j]: i行j列目にクイーンを置くかどうか
    var = [[Variable("%d %d" % (i, j)) for j in range(N)] for i in range(N)]
    
    # 各行には少なくともひとつのクイーンが置かれている
    for i in range(N):
        clause = Cnf()
        for j in range(N):
            clause = clause | var[i][j]
        formula = formula & clause

    # 各列には少なくともひとつのクイーンが置かれている
    for j in range(N):
        clause = Cnf()
        for i in range(N):
            clause |= var[i][j]
        formula &= clause
    
    import itertools
    
    # 各行には高々ひとつのクイーンが置かれている
    # = 各行のどの2マスを見ても，少なくとも一方はクイーンが置かれていない
    for i in range(N):
        for j1, j2 in itertools.combinations(range(N), 2):
            formula &= -var[i][j1] | -var[i][j2]

    # 各列には高々ひとつのクイーンが置かれている
    # = 各列のどの2マスを見ても，少なくとも一方はクイーンが置かれていない
    for j in range(N):
        for i1, i2 in itertools.combinations(range(N), 2):
            formula &= -var[i1][j] | -var[i2][j]
    
    # 斜め方向を見たとき高々ひとつのクイーンが置かれている
    for i in range(N):
        for j in range(N):
            # 南西方向
            for k in range(1, N):
                if i + k >= N or j - k < 0:
                    break
                formula &= -var[i][j] | -var[i + k][j - k]
            # 南東方向
            for k in range(1, N):
                if i + k >= N or j + k >= N:
                    break
                formula &= -var[i][j] | -var[i + k][j + k]

    solver = Minisat()
    solution = solver.solve(formula)

    print('== solution ==')
    for i in range(N):
        for j in range(N):
            if (solution[var[i][j]]):
                print('q', end='')
            else :
                print('.', end='')
        print()    

if __name__ == "__main__":
    solve(8)
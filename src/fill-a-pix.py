from satispy import Variable, Cnf
from satispy.solver import Minisat


# x行y列の隣接するマス(x, y自身も含む)をタプル(x', y')のリストとして返す関数(要実装)
def neighbor_cells(x, y, h, w):
    res = []
    # res.append((x', y'))のように書く
    for i in range(-1, 2): # -1, 0, 1とループする
        for j in range(-1, 2):
            if 0 <= x + i < h and 0 <= y + j < w:
                res.append((x + i, y + j)) 
    return res


# 与えられたlisで指定された変数のリストのちょうどk個がTrueになるようなCNFを返す(要実装)
# itertoolsを使うと便利
def exactly_k(var, lis, k):
    sub = Cnf()
    import itertools
    for tup in itertools.combinations(lis, k + 1):
        clause = Cnf()
        for i, j in tup:
            clause |= -var[i][j]
        sub &= clause
    for tup in itertools.combinations(lis, len(lis) - k + 1):
        clause = Cnf()
        for i, j in tup:
            clause |= var[i][j]
        sub &= clause
    return sub

def solve(board):
    h = len(board)
    w = len(board[0])

    formula = Cnf()
    # i行j列を黒で塗るかどうか
    var = [[Variable('%d %d' % (i, j)) for j in range(w)] for i in range(h)]

    # i行j列の周りのマス(自身も含む)に数字が書いてあれば，その数だけ黒く塗られるような制約をCNFに追加する(要実装)
    for i in range(h):
        for j in range(w):
            if board[i][j] != '.':
                formula &= exactly_k(var, neighbor_cells(i, j, h, w), int(board[i][j]))
    
    solver = Minisat()
    solution = solver.solve(formula)

    if solution.success:
        print("== solution ==")
        for i in range(h):
            for j in range(w):
                if solution[var[i][j]]:
                    print('■', end='')
                else :
                    print('□', end='')
            print()
    else :
        print("no solution")
                
if __name__ == "__main__":
    
    import sys
    board = []
    for line in sys.stdin:
        board.append(line.strip())

    print(*board, sep="\n")
    solve(board)